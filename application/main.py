from application.app import app
from application import view
from application.posts.blueprint import posts

if __name__ == '__main__':
    app.run(debug=True)

app.register_blueprint(posts, url_prefix='/blog')
