from flask import render_template

from application.app import app


@app.route('/')
def hello_world():
    full_name = 'Jath'
    return render_template('index.html', name=full_name)
