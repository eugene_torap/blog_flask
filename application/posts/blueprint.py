from flask import request, Blueprint, render_template, redirect, url_for

from application.app import db
from application.posts.forms import PostForm
from ..models import Post, Tag

posts = Blueprint('posts', __name__, template_folder='templates')


@posts.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']

        try:
            post = Post(title=title, body=body)
            db.session.add(post)
            db.session.commit()
        except Exception as ex:
            print(ex)

        return redirect(url_for('posts.index'))

    form = PostForm()
    return render_template('posts/create_post.html', form=form)


@posts.route('/')
def index():
    q = request.args.get('q', '')
    page = request.args.get('page', 1, type=int)

    if q:
        post_list = Post.query.filter(Post.title.contains(q) | Post.body.contains(q))
    else:
        post_list = Post.query.order_by(Post.created.desc())

    pages = post_list.paginate(page=page, per_page=4)
    return render_template('posts/index.html', pages=pages)


@posts.route('/<slug>')
def post_detail(slug):
    post = Post.query.filter(Post.slug == slug).first()
    tags = post.tags
    return render_template('posts/post_detail.html', post=post, tags=tags)


@posts.route('/tag/<slug>')
def tag_detail(slug):
    tag = Tag.query.filter(Tag.slug == slug).first()
    post_list = tag.posts.all()
    return render_template('posts/tag_detail.html', tag=tag, post_list=post_list)
